#!/bin/sh
set -e

a2enmod headers

echo "Run now orig entrypoint file"
sh /usr/local/bin/docker-php-entrypoint "$@"
