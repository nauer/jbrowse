#!/usr/bin/env bash

set -e;

# Remove sample data coming with jbrowse in PRODUCTION
#if [ -d "/jbrowse/jbrowse/sample_data" ];
#  then
#    echo "Remove Volvox sample data";
#    rm -r /jbrowse/jbrowse/sample_data;
#fi

# Your data
IN='/data';

# Processed Data (must be somewhere under /jb or you have update docker-compose.yml nginx entry)
OUT='/jb/json';

# Download sources
# if ! [ -f "$IN/reference/reference.fna" ];
#  then
#    echo "Download reference sequence";
#    curl -L ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_mammalian/Cricetulus_griseus/latest_assembly_versions/GCF_003668045.1_CriGri-PICR/GCF_003668045.1_CriGri-PICR_genomic.fna.gz | gunzip > $IN/reference/reference.fna;
# fi

# if ! [ -f "$IN/gff/anno.gff" ];
#   then
#     echo "Download annotation files";
#     curl -L ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_mammalian/Cricetulus_griseus/latest_assembly_versions/GCF_003668045.1_CriGri-PICR/GCF_003668045.1_CriGri-PICR_genomic.gff.gz | gunzip > $IN/gff/anno.gff;
# fi

#echo "Remove previous tracks"
#rm -r $OUT;

# uncomment the line below to compress all the datasets on disk
# (requires a little bit of web server configuration for the files to
# be served correctly)
# Compression works quite well with Apache webserver ~ 1/5 of size
# see https://jbrowse.org/docs/compression.html
COMPRESS='--compress';
CORES=4

#for f in $IN/reference/*.fa; do

add_reference() {
  # format the reference sequences
  echo "Add reference sequence $1" >> /logs/progress;

  # Get basename without suffixes

  name_w_suff=${f##*/}
  name=${name_w_suff%.*}

  prepare-refseqs.pl $COMPRESS --fasta $1 --out $OUT/$name >> /logs/progress;
  break;
}

find $IN/reference/ -name "*.fa" -type f -print0 | xargs -0 -n1 -P$CORES $add_reference

#done

for f in $IN/reference/*.fa; do
  name_w_suff=${f##*/}
  name=${name_w_suff%.*}

  echo "Add $IN/gff/anno.gff to $name Tracks" >> /logs/progress;

  flatfile-to-json.pl $COMPRESS \
  --out "$OUT/$name" \
  --gff "$IN/gff/anno.gff" \
  --autocomplete all \
  --type CDS,exon,gene,guide_RNA,lnc_RNA,miRNA,mRNA,primary_transcript,pseudogene,rRNA,snoRNA,snRNA,transcript,tRNA \
  --trackLabel "Genes"  \
  --trackType CanvasFeatures \
  --key "Genes" \
  --getSubfeatures    \
  --urltemplate "https://www.ncbi.nlm.nih.gov/gene/{name}" \
  --nameAttributes "name,alias,gene,custom" \
  --metadata "{\"description\": \"NCBI Genomic Annotation (RefSeq Release 93)\", \"source\": \"ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_mammalian/Cricetulus_griseus/latest_assembly_versions/GCF_003668045.1_CriGri-PICR/GCF_003668045.1_CriGri-PICR_genomic.gff.gz\"}" >> /logs/progress;
  break;
done

for f in $IN/reference/*.fa; do
  name_w_suff=${f##*/}
  name=${name_w_suff%.*}

  for f2 in $IN/bed/*.adj; do

    echo "Add $IN/bed/anno.gff to $name Tracks" >> /logs/progress;

    flatfile-to-json.pl $COMPRESS \
    --out "$OUT/$name" \
    --bed "$f2" \
    --trackLabel "Chromosome States"  \
    --trackType CanvasFeatures \
    --key "Chromosome_States" \
    --metadata "{\"description\": \"NCBI Genomic Annotation (RefSeq Release 93)\", \"source\": \"ftp://ftp.ncbi.nlm.nih.gov/genomes/refseq/vertebrate_mammalian/Cricetulus_griseus/latest_assembly_versions/GCF_003668045.1_CriGri-PICR/GCF_003668045.1_CriGri-PICR_genomic.gff.gz\"}" >> /logs/progress;
    break;
  done
  break;
done

# index feature names
# for f in $IN/reference/*.fa; do
#   name_w_suff=${f##*/}
#   name=${name_w_suff%.*}
#
#   echo "Create names for $name" >> /logs/progress;
#   generate-names.pl $COMPRESS --out "$OUT/$name";
#   break;
# done

echo "Finished" >> /logs/progress;
