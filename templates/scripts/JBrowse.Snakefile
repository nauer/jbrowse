#!/usr/local/bin/snakemake --snakefile

# JBrowse Build Script
#
# Author: Norbert Auer (norbert.auer@boku.ac.at)
# Copyright: Acib GmbH, Muthgasse 11, Vienna
# License: GPL v3
#
# Load and preprocess all input data of the JBrowse.

import sys
from pathlib import Path
import shutil
from subprocess import Popen, PIPE, call, check_output
#import logging
#import multiprocessing_logging

__version__ = "1.0"

workdir:
    "/jbrowse/jbrowse/"

# Set multiprocessing logging
# TODO multiprocess logging does not work with tail -f
#docker_out = "/logs/progress"

# handler = logging.FileHandler(docker_out)
# #handler.setFormatter(formatter)
#
# jb_logger = logging.getLogger('jbrowse')
# jb_logger.setLevel(logging.INFO)
# jb_logger.addHandler(handler)
#
# multiprocessing_logging.install_mp_handler(jb_logger)

wd_path = Path('.').resolve()


def get_files(path: str, glob: str='*'):
    return [str(child.resolve()) for child in Path(path).glob(glob)]


# Match .fa and .fasta
REFERENCES = get_files('/data/reference', "*.fa*")
REFERENCE_NAMES = [Path(Path(name).parts[-1]).stem for name in REFERENCES]


GFFS = get_files('/data/gff', "*.gff*")
GFF_NAMES = [Path(Path(name).parts[-1]).stem for name in GFFS]


rule config:
    message:
        "Update JBrowse config files"
    shell:
        """
            echo "Copy ./configs/jbrowse.conf"
            cp /configs/jbrowse.conf /jbrowse/jbrowse/
            echo "Copy ./configs/jbrowse_conf.json"
            cp /configs/jbrowse_conf.json /jbrowse/jbrowse/
        """

rule rm_sample_data:
    message:
        "Remove Volvox sample data"
    output:
        touch("__RM_SAMPLE_DATA__")
    shell:
        """
            if [ -d "/jbrowse/jbrowse/sample_data" ];
            then
                echo "Remove Volvox sample data" #>> {docker_out}
                rm -r /jbrowse/jbrowse/sample_data
            fi
        """

rule reset:
    message:
        "Reset and remove all datasets in jbrowse/data"
    output:
        touch("__RESET__")
    run:
        for child in wd_path.joinpath('data').iterdir():
            if child.is_dir():
                print("Remove dataset: {}!\n".format(str(child)))

                shutil.rmtree(str(child))


rule _add_reference:
    message:
        "Add Sequence Track: {wildcards.refs}"
    input:
        rules.reset.output,
        rules.config.output,
        fasta = "/data/reference/{refs}.fa"
    params:
        target = "data/{refs}"
    output:
        touch("__ADD_REF_{refs}__"),
        dataset = "data/{refs}/trackList.json"
    log:
        "/logs/progress"
    run:
        print("Add reference {}!".format(input.fasta))
        check_output(['bin/prepare-refseqs.pl', '--fasta', input.fasta, '--out', params.target])
        (Path(params.target) / "tracks.conf").write_text("[general]\ndataset_id = {}".format(wildcards.refs))


rule add_references:
    message:
        "Add Sequence Tracks"
    input:
        dataset = expand("data/{refs}/trackList.json", refs=REFERENCE_NAMES)
    output:
        touch("__ADD_REFS__")


rule _add_gff:
    message:
        "Add GFF Track: {wildcards.refs}"
    input:
        rules.add_references.output,
        gff = "/data/gff/{refs}.gff"
    params:
        target = "data/{refs}"
    output:
        touch("__ADD_GFF_{refs}__"),
        done = "data/{refs}/tracks/Genes/__GFF"
    run:
        #with open(docker_out, 'a') as docker:
        print("Add reference {}!".format(input.gff))
        check_output(['bin/flatfile-to-json.pl', '--gff', input.gff, '--tracklabel', 'Genes', '--out', params.target])
        (Path(params.target) / 'tracks/Genes/__GFF').touch()


rule add_GFFs:
    message:
        "Add GFF Tracks"
    input:
        rules.add_references.output,
        dataset = expand("data/{refs}/tracks/Genes/__GFF", refs=REFERENCE_NAMES)
    output:
        touch("__ADD_GFFS__")


rule run_custom_scripts:
    message:
        "Run all scripts from the scripts/custom folder"
    output:
        touch("__CUSTOM_SCRIPTS__")
    run:
        custom = Path('/scripts/custom')

        if not custom.is_dir():
            custom.mkdir()

        with open(docker_out, 'a') as docker:
            for child in custom.iterdir():
                print("Run {} in /scripts!\n".format(child.name))
                check_output([str(child.resolve())])
