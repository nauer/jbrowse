#!/usr/bin/env python3

import sys
from argparse import ArgumentParser
from pathlib import Path
from shutil import copytree, copy2

DEBUG = False

VERSION = 1.1

FOLDERS = {'configs', 'plugins', 'data', 'scripts', 'logs', 'jb'}


def start(args):
    # Check if directory exists
    project_path = Path(args.directory)

    if not project_path.is_dir():
        project_path.mkdir()

    # Create missing folders and copy template files into it
    for folder in FOLDERS:
        temp_path = project_path.joinpath(folder)
        if not temp_path.is_dir():
            copytree(str(Path('templates').joinpath(folder).absolute()), str(temp_path.absolute()))

    # Copy template env.sample file and docker-compose.yaml file
    copy2('env.sample', str(project_path.absolute().joinpath('.env')))
    copy2('docker-compose.yaml.sample', str(project_path.absolute().joinpath('docker-compose.yaml')))

    # Add project name variable to .env
    with project_path.absolute().joinpath('.env').open('a') as f:
        f.write('PROJECT_NAME={}'.format(project_path.parts[-1]))

    sys.stdout.write("Switch to the project folder and run 'docker-compose up'\n")


def main(argv=None):
    if argv is not None:
        sys.argv.extend(argv)

    # Setup argument parser
    parser = ArgumentParser()
    parser.add_argument('-V', '--version', action='version', version="Version: v{}".format(VERSION))
    parser.add_argument('directory', type=str, metavar='PATH', help="Path to project folder")

    # Process arguments
    args = parser.parse_args()
    start(args)


if __name__ == "__main__":
    if DEBUG:
        main(['test'])
    else:
        main()

