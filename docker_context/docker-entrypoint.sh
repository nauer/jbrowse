#!/bin/bash
#export JBROWSE=/jbrowse/

# Empty Log
echo "$(date)" >> /logs/progress
echo "------------------------------------------" >> /logs/progress
echo "Build JBrowse version: `printenv VERSION`" >> /logs/progress
echo "Mount /jb in yourt docker-compose.yml to get access to the Jbrowse files"
echo "Sync JBrowse data to /jb. This will take some time in the first run"

# Do not over write existing user configs
rsync --update --exclude={jbrowse.conf,jbrowse_conf.json,trackList.json} -a --quiet /jbrowse/jbrowse/ /jb

# Only copy user configs if not exist
rsync --ignore-existing -a --quiet /jbrowse/jbrowse/jbrowse_conf.json /jb
rsync --ignore-existing -a --quiet /jbrowse/jbrowse/jbrowse.conf /jb

# if [ ! -z "$(ls -A /plugins)" ]; then
#    echo "Copy plugins into jbrowse"
#    cp -r /plugins/* jbrowse/plugins
# fi

echo "JBrowse is ready!"  >> //logs/progress
echo "Snakemake version: $(snakemake --version)"  >> /logs/progress
echo "JBrowse version: $VERSION"  >> /logs/progress
echo "Workdir: $(pwd)" >> /logs/progress
echo "Website is running at: http://localhost:$APACHE_PORT/jb/index.html" >> /logs/progress

if [ -d "/jbrowse/jbrowse/sample_data" ];
then
    echo "Check Volvox sample data: http://localhost:$APACHE_PORT/jb/index.html?data=sample_data%2Fjson%2Fvolvox" >> /logs/progress
fi

echo "Run 'docker-compose exec jbrowse JBrowse.Snakefile --list' to see available Snakemake commands" >> /logs/progress
echo "" >> /logs/progress
echo "Put new data in ./data" >> /logs/progress
echo "Put new scripts in ./scripts" >> /logs/progress
#echo "See progress logs in ./logs/progress" >> /logs/progress
echo "Alter JBrowse.Snakefile in ./scripts to your purpose or add your own scripts into ./scripts/custom" >> /logs/progress
echo "Switch into JBrowse container with 'docker-compose exec jbrowse bash'" >> /logs/progress
#echo "Run 'docker-compose exec jbrowse JBrowse.Snakefile add_references' to build test jbrowse" >> /logs/progress
echo "Run 'docker-compose exec jbrowse JBrowse.Snakefile add_GFFs' to build test jbrowse" >> /logs/progress
echo "Result is shown at: http://localhost:$APACHE_PORT/jb/index.html?data=sample_data%2Fjson%2Fvolvox" >> /logs/progress
echo "Have Fun!" >> /logs/progress

# Show Build Log in docker-compose logs
tail -f /logs/progress
