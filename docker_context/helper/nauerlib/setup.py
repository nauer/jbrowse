from setuptools import setup, find_packages

setup(
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
    name="nauerlib",
    version="0.1",
    install_requires=["aiohttp", "asyncio"],
    package_dir={"": "src"},
    packages=find_packages("src", exclude=["contrib", "docs", "tests"]),
    url="https://gitlab.com/nauer/nauerlib",
    license="GPL 3.0",
    author="Norbert Auer",
    author_email="norbert@derauer.net",
    description="My personal library",
    classifiers=[  # Optional
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        "Development Status :: 3 - Alpha",
        # Indicate who your project is intended for
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Build Tools",
        # Pick your license as you wish
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
)
