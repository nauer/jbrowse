import pytest

#from async_downloader.dl import get_client

"""
    Unittesting mit pytest
    https://docs.pytest.org/en/latest/
    
    Alle Funktionen die getestet werden sollen müssen mit 'test' beginnen 
"""


# Einfacher pytest
# def test_multiply():
#     assert multiply(100, 3) == 300
#
#
# # Parameter für test_multiply (x, y, result)
# m_test = [(3, 5, 15),
#           (-3, -5, 15),
#           (0, 0, 0)]
#
# # Parameter für test_divide (x, y, result)
# d_test = [(15, 5, 3),
#           (-15, -5, 3),
#           (0, 1, 0)]
#
# # @pytest.mark.parametrize testet mehrere Parameter aufeinmal
# @pytest.mark.parametrize("x, y, result", m_test)
# def test_multiply_par(x, y, result):
#     assert multiply(x,y) == result
#
#
# @pytest.mark.parametrize("x, y, result", d_test)
# def test_divide_par(x, y, result):
#     assert divide(x,y) == result
#
#
# def test_divide_raises_ZeroDivisionError():
#     # Test Exception raising - Division durch 0 raises ZeroDivisionError
#     with pytest.raises(ZeroDivisionError):
#         divide(10,0)
