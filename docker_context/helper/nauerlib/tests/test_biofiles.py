import pytest
import pathlib

from biofiles import fasta

"""
    Unittesting mit pytest
    https://docs.pytest.org/en/latest/

    Alle Funktionen die getestet werden sollen müssen mit 'test' beginnen 
"""


class TestFastaFile(object):
    @pytest.fixture
    def F(self):
        f = fasta.FastaFile("../data/test_fasta.fa")
        yield f
        f.disconnect()

    # Test for FastaFile
    def test_FastaFile(self, F):
        assert F.file_path == pathlib.Path("../data/test_fasta.fa")

    def test_FastaFile_difference(self):
        with fasta.FastaFile("../data/test_fasta3.fa") as F1, fasta.FastaFile("../data/test_fasta4.fa") as F2:
            result = F1.difference(F2)

        assert result[0][0].header == ">seq5"
        assert result[1][0].header == ">seq2"
        assert len(result[0]) == 1
        assert len(result[1]) == 1

    def test_FastaFile_filter_by_header(self, F):
        result = list(F.filter_by_header("[14]"))

        assert result == [F.get_sequence(0), F.get_sequence(2), F.get_sequence(3), F.get_sequence(4), F.get_sequence(5)]

    def test_FastaFile_length(self, F):
        assert len(F) == 6

    def test_create_index(self, F):
        assert str(F.file_path) == "../data/test_fasta.fa"

    def test_get_sequence(self, F):
        assert F.get_sequence(0).header == ">seq1 duplicate seq"
        assert F.get_sequence(-1).header == ">seq4"

    def test_iter(self, F):
        for index, seq in enumerate(F):
            assert seq.header == F.get_sequence(index).header

    def test_with(self):
        with fasta.FastaFile("../data/test_fasta.fa") as F:
            assert F._handler is not None

        assert F._handler is None

    def test_mytest(self):
        with pytest.raises(FileExistsError):
            with fasta.FastaFile("not_exist.fa") as F:
                pass


class TestFastaSequence(object):
    def test_FastaSequence_equal(self):
        s1 = fasta.FastaSequence("seq1", "ATTG")
        s2 = fasta.FastaSequence("seq2", "ATTG")
        s3 = fasta.FastaSequence("seq3", "TTTG")
        s4 = fasta.FastaSequence("seq4", "attg")

        assert s1 == s2
        assert s1 != s3
        assert s1 != s4
