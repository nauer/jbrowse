from sys import stdout
from pathlib import Path

# TODO: Comments are not supported yet
# TODO: Check location sort during iterating
# TODO: Constaraint class for filtering gff entries


class GffEntry:
    __slots__ = (
        "parents",
        "children",
        "id",
        "_attributes",
        "seqid_str",
        "source_str",
        "type_str",
        "start",
        "end",
        "score_str",
        "strand_str",
        "phase_str",
    )

    def __init__(self, seqid, source, type, start, end, score, strand, phase, attributes):
        self.seqid_str = seqid
        self.source_str = source
        self.type_str = type
        self.start = int(start)
        self.end = int(end)
        self.score_str = score
        self.strand_str = strand
        self.phase_str = phase

        # Handle attributes
        self._attributes = {}
        self._read_attributes(attributes)
        self.id = self._attributes[b"ID"]

        self.parents = []
        self.children = []

    def __hash__(self):
        keys = list(self._attributes.keys())
        keys.sort()

        attr = []
        for key in keys:
            attr.append((key, self._attributes[key]))

        return hash(
            (
                self.seqid_str,
                self.source_str,
                self.type_str,
                self.start,
                self.end,
                self.score_str,
                self.strand_str,
                self.phase_str,
                tuple(attr),
            )
        )

    def __str__(self):
        attr = []

        for key, val in self._attributes.items():
            if key == b"Parent":
                val = b",".join(val)
                attr.append("{}={}".format(key.decode(), val.decode()))
            else:
                attr.append("{}={}".format(key.decode(), val.decode()))

        return str(
            "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(
                self.seqid_str.decode(),
                self.source_str.decode(),
                self.type_str.decode(),
                self.start,
                self.end,
                self.score_str.decode(),
                self.strand_str.decode(),
                self.phase_str.decode(),
                ";".join(attr),
            )
        )

    def _read_attributes(self, attributes):
        attributes = attributes.split(b";")

        for attr in attributes:
            key, val = attr.split(b"=")

            if key == b"Parent":
                values = tuple(val.split(b","))
                self._attributes[key] = values
            else:
                self._attributes[key] = val

    def get_attribute(self, key):
        # Encode str to byte if not byte already
        try:
            key = str(key).encode()
        except AttributeError:
            pass

        if key in self._attributes:
            return self._attributes[key].decode()

    def iter_children(self, constraint=None):
        for child in self.children:
            if constraint:
                yield child
            else:
                yield child

            yield from child.iter_children(constraint)

    def iter_parents(self):
        for parent in self.parents:
            yield parent
            yield from parent.iter_parent()

    def set_attribute(self, key, value):
        # Encode str to byte if not byte already
        try:
            value = str(value).encode()
        except AttributeError:
            pass

        try:
            key = str(key).encode()
        except AttributeError:
            pass

        self._attributes[key] = value


class Gff3File:
    def __init__(self, file_path):
        self._handler = None

        self.file_path = Path(file_path)

        if not self.file_path.exists():
            raise FileExistsError

        # All gff entries
        self._db = {}

        # All gff entries with no parent
        self._root = {}
        self.types = set()
        self.sources = set()

        self._create_gff_dom()

    def __getitem__(self, item):
        # Encode str to byte if not byte already
        try:
            item = item.encode()
        except AttributeError:
            pass

        return self._db[item]

    def __iter__(self):
        for root_gff in self._root:
            yield self._root[root_gff]
            yield from self._root[root_gff].iter_children()

    def __len__(self):
        length = 0

        for key in self._db:
            length += len(self._db[key])

        return length

    def get_connection(self):
        if not self._handler:
            # Exception works also in Python <3.6
            try:
                self._handler = open(self.file_path, "br")
            except TypeError:
                self._handler = open(str(self.file_path), "br")

        return self._handler

    def disconnect(self):
        if self._handler is not None:
            self._handler.close()
            self._handler = None

    def _create_gff_dom(self):
        h = self.get_connection()

        # Set file descriptor to file start
        h.seek(0, 0)

        self._db = {}

        for line in h:
            line = line.strip()
            if line.startswith(b"#"):
                pass  # print("Comment")
            else:
                gff = GffEntry(*line.split(b"\t"))

                if gff.id in self._db:
                    self._db[gff.id].append(gff)
                else:
                    self._db[gff.id] = [gff]

                self.types.add(gff.type_str)
                self.sources.add(gff.source_str)

        self.disconnect()

        # self._db -> {id : [gff1, gff2], id2 : [gff3]}

        # key for instances
        for id in self._db:
            # Go through gffs in dict level 2
            for gff in self._db[id]:

                # Add root gffs
                if b"Parent" not in gff._attributes:
                    self._root[hash(gff)] = gff
                else:
                    # Get all parent ids from gff
                    for parent_id in gff._attributes[b"Parent"]:
                        # Go through all gffs with parent id
                        for gff2 in self._db[parent_id]:
                            # Do crosslink parents and children
                            gff2.children.append(gff)
                            gff.parents.append(gff2)

    def remove_by_type(self, gff_type):
        """
        Remove gff type from dom and update children with new parent
        :param gff_type: str
        :return: None
        """

        self._remove_by(gff_type, "type")

    def _remove_by(self, value, col):
        # Encode str to byte if not byte already
        try:
            value = value.encode()
        except AttributeError:
            pass

        if col == "type":

            def _get_column(_gff):
                return _gff.type_str

        elif col == "source":

            def _get_column(_gff):
                return _gff.source_str

        else:
            raise TypeError

        delete = set()

        for key in self._db:
            for gff in self._db[key]:
                if _get_column(gff) == value:
                    for child in gff.children:
                        attr = list(child._attributes[b"Parent"])
                        attr.remove(gff.id)

                        for g in gff.parents:
                            if b"Parent" in g._attributes:
                                attr.extend(g._attributes[b"Parent"])

                        child._attributes[b"Parent"] = tuple(attr)

                        child.parents.remove(gff)

                        child.parents.extend(gff.parents)

                        if not child.parents:
                            self._root[hash(child)] = child

                    for parent in gff.parents:
                        parent.children.remove(gff)
                        parent.children.extend(gff.children)

                    delete.add(key)

                    try:
                        del self._root[hash(gff)]
                    except KeyError:
                        pass

        for key in delete:
            del self._db[key]

    def get_types(self):
        return self.types

    def get_sources(self):
        return self.sources

    def get_root_gffs(self):
        for _gff in self._root:
            yield self._root[_gff]

    def iter_constraint(self, constraint):
        for root_gff in self._root:
            yield self._root[root_gff]
            yield from self._root[root_gff].iter_children(constraint)

    def print_header(self, stream=stdout):
        print("##gff-version 3\n#!gff-spec-version 1.21", file=stream)


if __name__ == "__main__":
    gff_path = "/home/nauer/Projects/cgr-referencegenome.boku.ac.at2/data/gff/all.gff"

    GFF = Gff3File(gff_path)

    print(GFF.get_types())

    # Get chromosome number for seqid
    chromosomes = {}

    for gff in GFF:
        if gff.type_str == b"region":
            chr = gff.get_attribute("chromosome")
            seqid = gff.seqid_str

            chromosomes[seqid] = chr

    # Remove unwanted entries
    GFF.remove_by_type("region")
    GFF.remove_by_type("V_gene_segment")
    GFF.remove_by_type("cDNA_match")
    GFF.remove_by_type("match")
    GFF.remove_by_type("C_gene_segment")

    # Update other entries with chromosome id
    with open("/home/nauer/Projects/cgr-referencegenome.boku.ac.at2/data/gff/all_chr.gff", "w") as f_in:
        GFF.print_header(f_in)
        for gff in GFF:
            gff.set_attribute("chromosome", chromosomes[gff.seqid_str])
            print(gff, file=f_in)

    print(GFF["id2053"][0].parents)
    print(GFF["id2053"][0])
    # print(gff)

    # for index, gff in enumerate(GFF):
    #     print(gff.id, len(gff.children), len(gff.parents))
    #
    # print(index)
    # print(len(GFF))

    # print(GFF.get_sources())
    # print(GFF.get_types())

    # GFF.remove_by_type("miRNA")

    # l1 = []
    # l2 = []
    #
    # for _gff in GFF.get_root_gffs():
    #     l1.append(hash(_gff))
    #
    # print(l1[:5])
    # print(len(l1))
    # print(len(set(l1)))
    #
    # for _gff in GFF:
    #     l2.append(hash(_gff))
    #
    # print(l2[:5])
    # print(len(l2))
    # print(len(set(l2)))
    #
    # print(len(GFF))
    #
    # GFF.remove_by_type("exon")
    # for gffs in GFF["gene2106"]:
    #     for _gff in gffs.iter_children():
    #         _gff.set_attribute("chr", 3)
    #         print(_gff)
