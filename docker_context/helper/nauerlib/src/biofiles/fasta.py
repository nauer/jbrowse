import pickle
import hashlib
import math
import re
from pathlib import Path

bytes_table = bytes.maketrans(b"", b"")
delete = b"\n\r\t "


class Error(Exception):
    """Base class for exceptions in this module."""

    pass


class FastaLineLengthError(Error):
    """ Fasta line length must be > 0 """

    pass


class FastaLineLengthTypeError(Error):
    """ Fasta line length must be from type int """

    pass


class FastaSequence:
    def __init__(self, header, sequence, line_length=80):
        self.line_length = line_length
        self._hash = None
        self._header = None
        self._sequence = None

        # Translate to bytes if not
        try:
            self._orig_header = header.encode()
        except AttributeError:
            self._orig_header = header

        try:
            self._orig_sequence = sequence.encode()
        except AttributeError:
            self._orig_sequence = sequence

        self._normalize_sequence()
        self._normalize_header()
        self._hash_sequence()

    @property
    def line_length(self):
        return self._line_length

    @line_length.setter
    def line_length(self, value):
        if not isinstance(value, int):
            raise FastaLineLengthTypeError("Fasta line length must be from type int")
        elif value < 1:
            raise FastaLineLengthError("Fasta line length must be > 0")
        else:
            self._line_length = value

    def _normalize_sequence(self):
        # Exception works also in Python <3.6
        try:
            self._sequence = self._orig_sequence.translate(bytes_table, delete=delete)
        except TypeError:
            self._sequence = self._orig_sequence.translate(bytes_table, delete)

    def _normalize_header(self):
        self._header = self._orig_header.strip()

        if self._header[0] != 62:
            self._header = b">" + self._header

    def _hash_sequence(self):
        m = hashlib.md5()
        m.update(self._sequence)

        self._hash = m.hexdigest().encode()

    @property
    def hash(self):
        return self._hash

    @property
    def header(self):
        return self._header.decode()

    @header.setter
    def header(self, value):
        # Translate to bytes if not
        try:
            self._orig_header = value.encode()
        except AttributeError:
            self._orig_header = value

        self._normalize_header()

    @property
    def sequence(self):
        return self._sequence.decode()

    def set_line_length(self, length):
        self.line_length = length

    def __len__(self):
        return len(self._sequence)

    def __str__(self):
        seq = self._sequence.decode()

        seq = [
            seq[self.line_length * i : self.line_length * (i + 1)]
            for i in range(math.ceil(len(seq) / self.line_length))
        ]

        return "\n".join((self._header.decode(), "\n".join(seq)))

    def __bytes__(self):
        return b"\n".join((self._header, self._sequence))

    def __eq__(self, other):
        if isinstance(other, FastaSequence):
            return self._hash == other._hash
        else:
            raise TypeError


class FastaFile:
    def __init__(self, file_path, fasta_index=None):
        self._dict = {}
        self._index = []
        self._header_count = 0
        self._handler = None
        self._index_path = None

        self.file_path = Path(file_path)

        if not self.file_path.exists():
            raise FileExistsError

        if fasta_index is not None:
            self._index_path = Path(fasta_index)
            self.read_index(self._index_path)
        else:
            self.create_index()

    def disconnect(self):
        if self._handler is not None:
            self._handler.close()
            self._handler = None

    def filter_by_header(self, pattern, flags=0):
        """
        Return sequences matching the filter pattern
        :param pattern:
        :return:
        """
        p = re.compile(pattern, flags)

        for seq in self:
            if p.search(seq.header):
                yield seq

    def create_index(self):
        # Exception works also in Python <3.6
        try:
            self._handler = open(self.file_path, "br")
        except TypeError:
            self._handler = open(str(self.file_path), "br")

        pos = self._handler.tell()

        index = 0

        for line in self._handler:
            line = line.strip()

            if line.startswith(b">"):
                # Increment Sequence count
                self._header_count += 1

                # Update _index for sequential search
                self._index.append(pos)

                # Update _dict for header search
                if line[1:] in self._dict:
                    # Save index for further search in _index
                    self._dict[line[1:]].append(index)
                else:
                    self._dict[line[1:]] = [index]

                index += 1

            pos = self._handler.tell()

    def _search_index(self, header):
        if not self._dict:
            self.create_index()

        if header in self._dict:
            return self._dict[header]

    def write_index(self, index_path):
        if not self._dict:
            self.create_index()

        with open(index_path, "wb") as f_out:
            pickle.dump((self._dict, self._index, self._header_count), f_out)

    def difference(self, other):
        l1 = {}
        l2 = {}

        for seq in self:
            h = seq.hash

            if h in l1:
                l1[h].append(seq._header)
            else:
                l1[h] = [seq._header]

        for seq in other:
            h = seq.hash

            if h in l2:
                l2[h].append(seq._header)
            else:
                l2[h] = [seq._header]

        diff1 = {k: l1[k] for k in l1 if k not in l2}
        diff2 = {k: l2[k] for k in l2 if k not in l1}

        fasta_sequences1 = []
        fasta_sequences2 = []

        # Search in _dict index with header
        for val in diff1.values():
            for header in val:
                print(header)
                for i in self._search_index(header[1:]):
                    fasta_sequences1.append(self.get_sequence(i))

        for val in diff2.values():
            for header in val:
                for i in other._search_index(header[1:]):
                    fasta_sequences2.append(other.get_sequence(i))

        return (fasta_sequences1, fasta_sequences2)

    def read_index(self, index_path):
        with open(index_path, "rb") as f:
            self._dict, self._index, self._header_count = pickle.loads(f.read())

    def _read_sequence(self, start, end):
        f = self._open_sequence()

        f.seek(start)

        if end is None:
            return f.read()
        else:
            return f.read(end - start)

    def _open_sequence(self):
        if not self._handler:
            # Exception worka also in Python <3.6
            try:
                self._handler = open(self.file_path), "br"
            except TypeError:
                self._handler = open(str(self.file_path), "br")

        return self._handler

    def _create_fasta_sequence_object(self):
        pass

    def __getitem__(self, item):
        if isinstance(item, slice):
            # Convert slice to range object
            r = range(*item.indices(len(self._index)))

            for i in r:
                yield self.get_sequence(i)
        else:
            yield self.get_sequence(item)

    def get_sequence(self, index):
        """
        Because __get_item__ only returns a generator, get_sequence returns a FastaSequence directly
        :param index:
        :return: FastaSequence
        """
        if index < 0:
            index += len(self._index)

        if len(self._index) >= index + 2:
            seq = self._read_sequence(self._index[index], self._index[index + 1])
        else:
            seq = self._read_sequence(self._index[index], None)

        return FastaSequence(*seq.split(b"\n", maxsplit=1))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()

    def __len__(self):
        if not self._index:
            self.create_index()

        return len(self._index)

    def __iter__(self):
        if not self._index:
            self.create_index()

        for i in range(len(self._index)):
            yield self.get_sequence(i)


if __name__ == "__main__":
    # with FastaFile("../../data/test_fasta.fa") as F:
    #     F[1]
    #     print("No slice", F[1])
    #
    #     # for i in F[:5:2]:
    #     #    print(i)

    F = FastaFile("../../data/test_fasta.fa")

    for seq in F[1:3]:
        print(seq)

    for seq in F[:-1]:
        print(seq)

    print(next(F[-1]))
    print(F.get_sequence(0))
    print(F.get_sequence(-1))
    F.disconnect()
