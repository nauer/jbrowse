#!/usr/bin/env python3
# encoding: utf-8
"""
scrap_vbcf - A HTML scrapper for the Vienna Biocenter download site

@author:     Norbert Auer

@copyright:  2018 ACIB GmbH, Vienna. All rights reserved.

@license:    GPL 3.0

@contact:    norbert.auer@acib.at
"""

import sys

from pathlib import Path
import logging
from argparse import ArgumentParser, FileType
from async_downloader import dl
import asyncio
from lxml import html
import datetime
import hashlib

DEBUG = False

__version__ = "v0.6"
prog_name = "vbcf_downloader"
description = "A HTML download scrapper for the Vienna Biocenter download site"

base_url = "https://ngs.vbcf.ac.at/"


def start(args):
    # Get async event loop
    loop = asyncio.get_event_loop()

    def _login(loop, base_url, data_login):
        # Get Session
        session = loop.run_until_complete(loop.create_task(dl.get_session(None)))
        print(session)

        # Get Login session
        loop.run_until_complete(
            loop.create_task(
                dl.get_login_session(session, base_url + "login", data_login)
            )
        )
        print(session)
        return session

    logging.debug("Start")

    if args.samples:
        print("Samples")

        data_login = {
            "username": args.login,
            "password": args.passwd,
            "requestedurl": "noredirect",
        }

        samples = []

        for line in args.samples:
            if line.strip() == "" or line.strip().startswith("#"):
                continue

            sample, delimiter, url = line.partition("\t")

            samples.append({"sample": sample.strip(), "url": url.strip()})

            logging.info("Get download links for {}".format(sample))
        print(samples)
        #        exit()

        # print(samples)

        try:
            # Get Login session
            print("Before Login", datetime.datetime.now())
            session = _login(loop, base_url, data_login)
            print("After Login", datetime.datetime.now())
            commands = []

            for sample in samples:
                commands.append(dl.get_url(session, sample["url"], {}))

            download_urls = []

            # Run all commands
            for result in loop.run_until_complete(asyncio.gather(*commands)):
                tree = html.fromstring(result)

                for path in tree.xpath(
                    "//table[@id='filesTable']/tbody/tr/td[9]/a/@href"
                ):
                    download_urls.append(base_url + path.strip("/"))

                for path in tree.xpath("//div[@class='qcMenu']//a/@href"):
                    download_urls.append(base_url + path.strip("/"))

            # Create output dir
            Path(args.output).mkdir(exist_ok=True, parents=True)

            logging.info("Start Download")
            dl.async_download_urls(
                loop,
                urls=download_urls,
                params=None,
                session=session,
                target_dir=args.output,
                prefix=None,
            )
            print("Ende", datetime.datetime.now())
        finally:
            # Close session and loop
            loop.run_until_complete(session.close())
            loop.close()

    else:
        print("Get")

        data_login = {
            "username": args.login,
            "password": args.passwd,
            "requestedurl": "noredirect",
        }

        try:
            # Get Login session
            session = _login(loop, base_url, data_login)

            # Run single command
            result = loop.run_until_complete(
                asyncio.gather(
                    dl.get_url(
                        session,
                        "{base}user/{user}".format(base=base_url, user=args.user),
                        {},
                    )
                )
            )

            print(result)
            tree = html.fromstring(result[0])

            for path in tree.xpath("//table[@id='sampletable']/tbody/tr/td[2]/a"):
                print(path.text, base_url + path.attrib["href"].strip("/"))
                # Write out result file
                args.get.write(
                    "{}\t{}{}\n".format(
                        path.text, base_url, path.attrib["href"].strip("/")
                    )
                )
        finally:
            # Close session and loop
            loop.run_until_complete(session.close())
            loop.close()


def setup_logger(log_level):
    """
    Setup logger for the main program. Use name instead of module to see hirachy.
    To activate logger in modules use this:
        import logging

        logging.getLogger(__name__).addHandler(logging.NullHandler())
    :param log_level: String
    :return: None
    """
    # create the logging file handler
    fh = logging.StreamHandler()
    formatter = logging.Formatter(
        "%(asctime)s - %(module)s - %(levelname)s - %(message)s"
    )
    fh.setFormatter(formatter)

    logger = logging.getLogger()
    logger.addHandler(fh)

    logger.setLevel(log_level)


def main(argv=None):
    """ Main function """
    if argv is not None:
        sys.argv.extend(argv)

    try:
        # Setup argument parser
        parser = ArgumentParser(description=description, prog=prog_name)
        parser.add_argument("-V", "--version", action="version", version=__version__)
        parser.add_argument("-q", "--quiet", action="store_true", help="Turn off log")
        parser.add_argument(
            "-l",
            "--log-level",
            choices=["ERROR", "WARNING", "INFO", "DEBUG"],
            type=str,
            default="WARNING",
            help="Set the log level",
        )

        m_group = parser.add_mutually_exclusive_group(required=True)
        m_group.add_argument(
            "-s", "--samples", type=FileType("r"), help="Download all urls from file"
        )
        m_group.add_argument(
            "-g",
            "--get",
            type=FileType("w"),
            help="Save all available sources in filename.",
        )
        parser.add_argument(
            "-o", "--output", type=str, default=".", help="Output directory (default .)"
        )
        parser.add_argument(
            "user",
            type=str,
            metavar="USERNAME",
            help="Vienna Biocenter platfform username (Max.Mustermann)",
        )
        parser.add_argument(
            "login",
            type=str,
            metavar="USERLOGIN",
            help="Vienna Biocenter login username (Max.Mustermann@boku.ac.at)",
        )
        parser.add_argument(
            "passwd",
            type=str,
            metavar="PASSWORD",
            help="Vienna Biocenter login password",
        )

        # Process arguments
        args = parser.parse_args()

        if args.quiet:
            args.log = logging.NOTSET
        else:
            # Create logger
            setup_logger(args.log_level)

        logging.debug("Argparse args: {}".format(args))

        return start(args)
    except KeyboardInterrupt:
        ### Handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(prog_name) * " "
        sys.stderr.write(prog_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        raise e


if __name__ == "__main__":
    argv = []
    if DEBUG:
        # argv.append("-v")
        argv.append("-l")
        argv.append("DEBUG")
        argv.append("-s")
        argv.append("links")
        argv.append("-o")
        argv.append("/home/nauer/Projects/workdir")
        argv.append("martina.baumann")
        argv.append("martina.baumann@boku.ac.at")
        argv.append("Ad#g37Pp")

    sys.exit(main(argv))
