import curses


def get_input(window, y, x, render_static=None):
    inp = []
    event = None

    window.move(y, x)
    window.deleteln()
    window.insertln()

    if render_static is not None:
        render_static()

    window.move(y, x)

    # Echo user input
    while event != "\n":
        event = window.getkey()
        inp.append(event)
        window.addstr(y, x, "".join(inp))

    return "".join(inp)


def get_passwd(window, y, x, chr="", render_static=None):
    inp = []
    passwd = []
    event = None

    window.move(y, x)
    window.deleteln()
    window.insertln()

    if render_static is not None:
        render_static()

    window.move(y, x)

    # Echo user input
    while event != "\n":
        event = window.getkey()
        passwd.append(chr)
        inp.append(event)
        window.addstr(y, x, "".join(passwd))

    return "".join(inp)


if __name__ == "__main__":
    from curses import wrapper

    def win1_static(win):
        def static():
            win.addstr(1, 2, "Static Text", curses.A_BOLD)
            win.addstr(2, 3, "X:")
            win.addstr(5, 3, "Y:")
            win.border(0)

        return static

    def win2_static(win):
        def static():
            win.addstr(1, 2, "Static Text", curses.A_BOLD)
            win.addstr(2, 3, "Password:")
            win.border(0)

        return static

    def main(stdscr):
        # Clear screen
        stdscr.clear()
        stdscr.addstr(1, 1, "Titel", curses.A_STANDOUT)
        stdscr.addstr(1, 7, "Result: ")
        stdscr.refresh()

        window1 = curses.newwin(10, 30, 2, 5)
        window2 = curses.newwin(5, 20, 2, 35)

        for i in range(3):
            stdscr.addstr(
                1, 15, get_input(window1, 2, 6, render_static=win1_static(window1))
            )
            stdscr.refresh()
            stdscr.addstr(
                1, 15, get_input(window1, 5, 6, render_static=win1_static(window1))
            )
            stdscr.refresh()

        for i in range(3):
            stdscr.addstr(
                1,
                15,
                get_passwd(window2, 2, 13, chr="*", render_static=win2_static(window2)),
            )
            stdscr.refresh()

    wrapper(main)
