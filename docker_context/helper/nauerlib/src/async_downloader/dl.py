import aiohttp
import asyncio
import hashlib
import pathlib
import logging
import requests
import sys
from os import path

__version__ = "0.1"


logging.getLogger(__name__).addHandler(logging.NullHandler())


async def get_session(timeout=None):
    # Unfortunately, total=None is not working
    # Set timeout to very high value
    if not timeout:
        timeout = 100000
    tout = aiohttp.ClientTimeout(total=timeout)

    logging.info("Create Client Session")
    return aiohttp.ClientSession(timeout=tout)


async def get_login_session(session, url, data, params=None):  # , timeout=None):
    # timeout = aiohttp.ClientTimeout(total=timeout)

    logging.info("Try to log in")
    async with session.post(url, data=data, params=params) as response:
        if response.status != 200:
            raise ConnectionError

    logging.info("Successfully logged in")


async def get_url(session, url, param):
    """
    Get url and returns content
    :param session:
    :param url:
    :param param:
    :return: str
    """
    async with session.get(url, params=param) as response:
        if response.status == 200:
            logging.info("Download of: {}".format(url))
            return await response.text()
        else:
            logging.warning("Download failed of: {} - {}".format(url, response.status))
            return response.status

        # content = await response.content.read()


async def download_url(
    session,
    url,
    param,
    csum="md5",
    chunk_size=100,
    target_dir=".",
    filename=None,
    prefix=None,
):
    """
    Async download url and generate sha1 or md5 checksum while downloading

    :param session:
    :param url:
    :param param:
    :param csum:
    :param chunk_size:
    :param target_dir:
    :param filename:
    :param prefix:
    :return:
    """
    if filename is None:
        filename = path.basename(url)
        print("FILENAME: ", filename)

    # Avoid overwriting files with same basename
    if prefix is None:
        link_hasher = hashlib.blake2b(key=b"dl", digest_size=4)
        link_hasher.update(path.dirname(url).encode())

        # Same url but different parameter
        for key, val in param.items():
            link_hasher.update(key.encode())
            link_hasher.update(val.encode())

        prefix = link_hasher.hexdigest() + "_"

    test_file = pathlib.Path(path.join(target_dir, prefix + filename))

    print("---> ", test_file, " <---")
    print(
        "---> ",
        test_file.with_suffix(".md5"),
        test_file.with_suffix(".md5").exists(),
        " <---",
    )
    if test_file.exists():
        if pathlib.Path(str(test_file) + ".md5").exists():
            logging.info(
                "Skip download. File {} already exists".format(
                    str(test_file.absolute())
                )
            )
            return
        else:
            logging.info(
                "File exists but no md5 file. Repeat download of file {}".format(
                    str(test_file.absolute())
                )
            )

    # Get target path
    target = pathlib.Path(target_dir).absolute()

    try:
        print("************** ->", url, param)
        async with session.get(url, params=param) as response:
            print("************ ", response.status)
            if response.status == 200:
                if csum == "md5":
                    hash = hashlib.md5()
                elif csum == "sha256":
                    hash = hashlib.sha256()
                else:
                    raise AttributeError

                logging.info(
                    "Download file {} to {}".format(
                        url, path.join(target_dir, prefix + filename)
                    )
                )
                with open(path.join(target_dir, prefix + filename), "wb") as f_data:

                    # chunk is holding the memory consuption low
                    try:
                        while True:
                            # print('A')
                            chunk = await response.content.read(chunk_size)
                            # print("Await:", url, len(chunk))
                            if not chunk:
                                logging.info(
                                    "Downloaded file {} to {} - (md5) {}".format(
                                        url,
                                        path.join(target_dir, prefix + filename),
                                        hash.hexdigest(),
                                    )
                                )
                                with open(
                                    path.join(target_dir, prefix + filename + ".md5"),
                                    "w",
                                ) as f_hash:
                                    f_hash.write(hash.hexdigest())

                                break

                            # Writes chunk
                            f_data.write(chunk)
                            # print('B')

                            # Updates the csum
                            hash.update(chunk)
                            # print('C')
                    except:
                        print("ERROR:", sys.exc_info())
                        raise

                # Create file hierarchy with links

                # Get download pathname
                pathname = pathlib.Path(target_dir) / pathlib.Path(prefix + filename)
                print("pathname ->", pathname)

                # Get download filename.md5
                filename_md5 = pathlib.Path(target_dir) / pathlib.Path(
                    prefix + filename + ".md5"
                )

                # Create folders
                parsed_url = pathlib.Path(
                    requests.urllib3.util.parse_url(url).path.strip("/")
                )
                print(url, parsed_url)

                # Linkname
                # link = target / pathlib.Path(prefix + str(parsed_url)) #pathlib.Path(prefix + filename)
                # link_md5 = target / parsed_url + '.md5' #pathlib.Path(prefix + filename + '.md5')

                # pp = target / parsed_url
                # print("pp", pp)
                # link = pp
                # link_md5 = pathlib.Path(str(pp) + '.md5')
                # print("link", link)
                # logging.info("Create Folder: {}".format(pp.parent))
                # pp.parent.mkdir(parents=True, exist_ok=True)
                #
                # # Create Links
                # print("Create Link: {} -> {}".format(str(link.absolute()), str(pathname.absolute())))
                # logging.info("Create Link: {} -> {}".format(str(link.absolute()), str(pathname.absolute())))
                #
                # try:
                #     link.symlink_to(pathname, target_is_directory=False)
                #     link_md5.symlink_to(filename_md5, target_is_directory=False)
                # except:
                #     logging.warning("Link already exists: {} - Error: {}".format(url, sys.exc_info()))
            else:
                logging.warning(
                    "Download failed of: {} - {}".format(url, response.status)
                )
    except:
        logging.warning(
            "Download failed of: {} - Error: {}".format(url, sys.exc_info())
        )

        with open(target / "error.out", "a") as f_error:
            f_error.write("{} - {}\n".format(url, sys.exc_info()[0]))


def async_download_urls(
    loop,
    urls,
    params=None,
    login_url=None,
    login_data=None,
    session=None,
    login_param=None,
    csum="md5",
    chunk_size=100,
    target_dir=".",
    prefix=None,
):
    try:
        if session is None:
            print("No Session yet")
            _session = loop.run_until_complete(loop.create_task(get_session()))
        else:
            print("Got Session")
            _session = session

        # Login if needed
        if login_data is not None:
            print("Login")
            loop.run_until_complete(
                loop.create_task(
                    get_login_session(session, login_url, login_data, login_param)
                )
            )
            print("Login was successful")

        if params is None:
            params = []
            [params.append({}) for i in range(len(urls))]

        count = 16
        print("Start Downloads: (Max {} tasks at once)".format(count))

        tasks = []

        for index, url_par in enumerate(zip(urls, params)):
            url = url_par[0]
            par = url_par[1]

            print(
                "{}/{} - Prepare Download for {}".format(index, index + 1 % count, url)
            )
            print(session, url, par, csum, chunk_size, target_dir, prefix)
            tasks.append(
                asyncio.Task(
                    download_url(
                        session=session,
                        url=url,
                        param=par,
                        csum=csum,
                        chunk_size=chunk_size,
                        target_dir=target_dir,
                        prefix=prefix,
                    )
                )
            )
            print("{}/{}".format(index, (index + 1) % count))

            if (index + 1) % count == 0:
                print(
                    "-------------------- Run Tasks {} --------------------".format(
                        index
                    )
                )
                loop.run_until_complete(asyncio.wait(tasks))
                tasks = []
                print("-------------------- Group Finished ------------------------")
    finally:
        if session is None:
            loop.run_until_complete(_session.close())


# def download_urls(urls=[], params=[], login_param=None, login_user=None, login_password=None):
#     for url, param in zip(urls, params):
#         html = requests.get(url, param)
#         print(html.content[:1000])


if __name__ == "__main__":
    import time

    urls = [
        "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi",
        "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi",
        "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi",
    ]
    params = [
        {"db": "nucleotide", "id": "24475906", "rettype": "acc"},
        {"db": "nucleotide", "id": "224465210", "rettype": "acc"},
        {"db": "nucleotide", "id": "50978625, 9507198", "rettype": "acc"},
    ]

    start = time.time()
    async_download_urls(urls, params)
    end = time.time()
    print(end - start)

    # start = time.time()
    # download_urls(urls, params)
    # end = time.time()
    # print(end - start)

    # Vienna Biocenter Login Data
    import json

    data = json.load(open("../../secret_cred", "r"))
    base_url = "https://ngs.vbcf.ac.at/"

    loop = asyncio.get_event_loop()
    session = loop.run_until_complete(loop.create_task(get_session(None)))
    loop.run_until_complete(
        loop.create_task(get_login_session(session, base_url + "login", data))
    )
    # Gather uname and date commands
    commands = asyncio.gather(get_url(session, base_url + "user/martina.baumann", {}))
    # Run the commands
    result = loop.run_until_complete(commands)
    # Print a report
    print(result)

    # loop.run_until_complete(loop.create_task())
    loop.run_until_complete(session.close())
    loop.close()
    # async_download_urls([base_url + "user/martina.baumann"], [{}], base_url + "login", data)
