


# Get unique chromosome nummbers
grep ">" picr_scaffolds.fa | grep -oP "[^=[\]]*]" | sort -u | sed 's/.$//' > unique_chromosomes.txt

# Filter sequences by chromosome
cat unique_chromosomes.txt | xargs -P 6 -n 1 -i fastagrep -e '={}' -o 'picr_scaffolds_chr_{}.fa' picr_scaffolds.fa
