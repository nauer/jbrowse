#!/usr/bin/env python3
# encoding: utf-8
"""
AppName - Description

@author:     Norbert Auer

@copyright:  2017 ACIB GmbH, Vienna. All rights reserved.

@license:    GPL 3.0

@contact:    norbert.auer@boku.ac.at
"""

import sys
import re

import json_path as jp

from distutils.util import strtobool
from argparse import ArgumentParser, RawDescriptionHelpFormatter

program_version_message = "v1.0"
program_license = ""

p_field_index = re.compile("([^[]*)((?:\[[-?\d]+\])*)")
p_indices = re.compile("\[([^]])\]")
p_curly = re.compile("{([^}])}")


def start(args):
    data = jp.read_json_file(args.file)

    result = jp.get_value(args.pattern, data, force=True)

    if args.value:
        if args.type == 'float':
            args.value = float(args.value)
        elif args.type == 'int':
            args.value = int(args.value)
        elif args.type == 'boolean':
            args.value = bool(strtobool(args.value))
        elif args.type == 'json':
            args.value = jp.read_json_string(args.value)

        if 'results' in result:
            for r in result['results']:
                r['parent'][r['parent_key']] = args.value
        else:
            result['parent'][result['parent_key']] = args.value

        jp.write_json_file(args.file, data, sort_keys=False, indent=args.indent, compact=args.compact)
    else:
        if 'results' in result:
            print(result['results'])
        else:
            print(result['result'])


def main(argv=None):
    """ Command line options """
    if argv is None:
        argv = sys.argv[1:]
    else:
        sys.argv.extend(argv)

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument('-p','--pattern', help="Search path to use in the JSON object. If path does not exist in the JSON object it will be created", type=str)
        parser.add_argument('-v', '--value', help="The value. If not defined The searched tag is printed", type=str, default='')
        parser.add_argument('-t', '--type', help="Type of the value. Default: string (string|int|float|boolean)", choices=['string', 'int', 'float', 'boolean', 'json'], type=str, default="string")
        parser.add_argument('-i', '--indent', help="Set the used indent", type=int, default=2)
        parser.add_argument('-c', '--compact', help="If <compact> format is defined <indent> is ignored", action='store_true')
        parser.add_argument('file', type=str, help="Path to file")


        #parser.add_argument('-s', '--sub', help="", type=str)

        # Process arguments
        # args = parser.parse_args(['--value', '10', '-t', 'int', '-p', 't[?]="5"', 'test.json'])
        args = parser.parse_args()


        return start(args)
    except KeyboardInterrupt:
        ### Handle keyboard interrupt ###
        return 0
    except Exception as e:
        indent = len(__name__) * " "
        sys.stderr.write(__name__ + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        raise e

if __name__ == "__main__":
    sys.exit(main())


# f1.f2[?]{label=='x'} # return first f1.f2[?]
# f2.?.[?]{label=='x'} # return first f2.?.[?]
# f1.f2{[?]=='x'}      # return first f1.f2
# f1.f2{[?]=='x'}[2]   # ???