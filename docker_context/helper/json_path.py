#!/usr/bin/env python3
# encoding: utf-8
"""
json_path - JSON Helper Library

@author:     Norbert Auer

@copyright:  2017 ACIB GmbH, Vienna. All rights reserved.

@license:    GPL 3.0

@contact:    norbert.auer@boku.ac.at
"""

import sys
import json
import re
from collections import OrderedDict

__version__ = "1.0"


class ParseResult:
    def __init__(self, result, next_tag):
        self.next_tag = next_tag
        self.result = result

    def __str__(self):
        return "Next Tag:{}\tResult:{}".format(self.next_tag, self.result)

    def __iter__(self):
        yield self.next_tag
        yield self.result


class JPResult:
    def __init__(self, data, parent, result=None, force=False):
        self._data = data
        self._parent = parent
        self._result = result
        self._force = force

    def is_forced(self):
        return self._force

    def set_result(self, result):
        self._result = result


p_indices = re.compile("\[([^]]+)\]\.?(.*)")
p_predicate = re.compile("{([^}]*)}\.?(.*)")
p_field = re.compile("([?\w]*)\.?(.*)")
p_quote = re.compile("""['"]([^'"]*)["']""")

p_equal = re.compile("(\S*)\s*==\s*([\w\s.]*)")
p_val_equal = re.compile("(\S*)\s*=\s*(.*)") # ("(\S*)\s*=\s*([\"'\w\s.]*)")
p_lower_equal = re.compile("(\S*)\s*<=\s*(\S*)")
p_greater_equal = re.compile("(\S*)\s*>=\s*(\S*)")
p_lower = re.compile("(\S*)\s*<\s*(\S*)")
p_greater = re.compile("(\S*)\s*>\s*(\S*)")
p_not_equal = re.compile("(\S*)\s*!=\s*(\S*)")

# Error Codes
ERROR_NOT_JSON = 1


def read_json_file(path):
    """
    Read json from path and returns OrderedDict
    :param path: str
    :return: OrderedDict
    """
    try:
        with open(path, 'r') as f:
            data = json.load(f, object_pairs_hook=OrderedDict)
    except FileNotFoundError:
        with open(path, 'w') as f:
            f.write("{}")
        data = OrderedDict()
        print("File does not exist. Create File <{}>".format(path), file=sys.stdout)
    except json.JSONDecodeError:
        print("File <{}> is empty or not a json.".format(path), file=sys.stdout)
        print(ERROR_NOT_JSON, file=sys.stderr)
        exit(ERROR_NOT_JSON)
    return data


def read_json_string(json_string):
    """
    Read json from str and returns OrderedDict
    :param json_string: str
    :return: OrderedDict
    """
    try:
        data = json.loads(json_string, object_pairs_hook=OrderedDict)
    except json.JSONDecodeError:
        print("String <> is empty or not a valid json.".format(json_string), file=sys.stdout)
        print(ERROR_NOT_JSON, file=sys.stderr)
        exit(ERROR_NOT_JSON)
    return data


def write_json_file(path, data, sort_keys=True, indent=2, compact=False):
    """
    Write OrderedDict to JSON. Sort keys and pretty write to file in JSON format. If compact is True indent is ignored.
    :param path: str
    :param data: OrderedDict
    :param sort_keys: boolean
    :param indent: int >= 0
    :param compact: boolean
    :return: str
    """
    if compact:
        j = json.dumps(data, sort_keys=sort_keys, separators=(',', ':'))
    else:
        j = json.dumps(data, sort_keys=sort_keys, indent=indent)

    with open(path, 'w') as f:
        f.write(j)

    return j


def parse_field(tag):
    try:
        field, next_tag = p_field.match(tag).groups()

        if field == '':
            field = None

        return ParseResult(field, next_tag)
    except:
        return ParseResult(None, tag)

def parse_index(tag):
    try:
        # index could be number, - or ?
        index, next_tag = p_indices.match(tag).groups()

        return ParseResult(index, next_tag)
    except:
        return ParseResult(None, tag)


def parse_predicate(tag):
    """
    Returns next_tag
    :param tag:
    :param result:
    :return:
    """
    try:
        predicate, next_tag = p_predicate.match(tag).groups()

        return ParseResult(next_tag=next_tag, result=predicate)
    except:
        return ParseResult(next_tag=tag, result=None)


def parse_operator(tag):
    if "=" in tag:
        r = p_val_equal.match(tag).group(2)

        r2 = p_quote.match(r)

        # TODO: Allow further next_tag
        if r2:
            return ParseResult(r2.group(1), '')
        else:
            return ParseResult(int(r), '')
    else:
        return ParseResult(None, '')


def get_value(tag, data, force=False):
    return _get_value(tag, data, parent=None, force=force)


def _get_value(tag, data, parent=None, parent_key=None, force=False):
    is_predicate = True
    predicate_okay = True

    next_tag = tag
    sub_data = data

    # Parse Predicates
    while is_predicate:
        parse_predicate_result = parse_predicate(next_tag)
        next_tag, predicate = parse_predicate_result

        # If no predicate
        if predicate is None:
            break

        result = _get_value(predicate, sub_data, parent=None, parent_key=None, force=force)

        # Predicate fails
        if result['result'] is None or result['result'] is False:
            # No further processing necessary
            return {'result': None, 'next_tag': next_tag}
            #predicate_okay= False#{'result': False, 'next_tag': next_tag}

    if not predicate_okay:
        return None

    # Parse Field
    parse_field_result = parse_field(next_tag)
    next_tag, field = parse_field_result

    # print(parse_field_result)

    if field is not None:
        if field == "?":
            group = {'results': [], 'next_tag': next_tag}

            for key in sub_data:
                parent = sub_data

                result = _get_value(next_tag, sub_data[key], parent=parent, parent_key=field, force=force)

                if result['result'] is not None:
                    group['results'].append(result['result'])

            return group

        # Detect field
        elif isinstance(sub_data, dict) and field in sub_data:
            pass
        elif force:
            if isinstance(sub_data, dict):
                sub_data[field] = {}
            else:
                parent[parent_key] = {}
                parent[parent_key][field] = {}
                sub_data = parent[parent_key]
        else:
            parent = sub_data

            # Break
            return {'result': None, 'parent': parent, 'parent_key': parent_key, 'next_tag': next_tag}

        parent = sub_data
        sub_data = sub_data[field]

        result = _get_value(next_tag, sub_data, parent=parent, parent_key=field, force=force)

        #if force:
        #    if result['result'] is None or result['result'] is False:
                
        # Result from Predicate
        # if result is True:
        #     return sub_data
        # elif result is False:
        #     return {'result': None, 'next_tag': next_tag}

        if 'error' in result:
            raise

        return result

    # Parse Index
    parse_index_result = parse_index(next_tag)
    next_tag, index = parse_index_result

    # print(parse_index_result)

    if index is not None:
        if index == "-":
            #if sub_data is None:
            if not isinstance(sub_data, list):
                if force:
                    parent[parent_key] = [None]


            parent = parent[parent_key]
            sub_data = parent[-1]

            result = _get_value(next_tag, sub_data, parent=parent, parent_key=-1, force=force)

            if 'error' in result:
                raise

            return result

        elif index == "+":
            if not isinstance(sub_data, list):
                parent[parent_key] = [None]
            else:
                sub_data.append(None)

            parent = parent[parent_key]
            sub_data = parent[-1]

            result = _get_value(next_tag, sub_data, parent=parent, parent_key=-1, force=force)

            if 'error' in result:
                raise

            return result

        elif "?" in index:
            group = {'results':[], 'next_tag': next_tag}

            if "==" in index:
                def func(x): return x == int(p_equal.match(index).group(2))
            elif ">=" in index:
                def func(x): return x >= int(p_greater_equal.match(index).group(2))
            elif "<=" in index:
                def func(x): return x <= int(p_lower_equal.match(index).group(2))
            elif "!=" in index:
                def func(x): return x != int(p_not_equal.match(index).group(2))
            elif ">" in index:
                def func(x): return x > int(p_greater.match(index).group(2))
            elif "<" in index:
                def func(x): return x < int(p_lower.match(index).group(2))
            elif "=" in index:
                def func(x): return x == p_val_equal.match(index).group(2)
            else:
                def func(x): return True

            for next_index, i in enumerate(sub_data):
                if func(next_index):
                    parent = sub_data

                    result = _get_value(next_tag, i, parent=parent, parent_key=next_index, force=force)

                    if 'error' in result:
                        raise

                    if result['result'] is not None:
                        group['results'].append(result)

            return group
        else:
            try:
                index = int(index)
            except ValueError:
                return {'result': index, 'parent': parent, 'parent_key': parent_key, 'next_tag': next_tag, 'error': index + 'not allowed'}

            if isinstance(sub_data, list) and len(sub_data) > index:
                pass
            elif force:
                if isinstance(sub_data, list):
                    sub_data.extend((index - len(sub_data) + 1) * [None])
                elif sub_data is None:
                    parent[parent_key] = list()
                    parent[parent_key].extend([None] * (index + 1))
                    sub_data = parent[parent_key]
                else:
                    parent[parent_key] = [None] * (index + 1)
                    sub_data = parent[parent_key]
            else:
                # Break
                return {'result': None, 'parent': parent, 'parent_key': parent_key, 'next_tag': next_tag}

            parent = sub_data
            sub_data = sub_data[index]

            result = _get_value(next_tag, sub_data, parent=parent, parent_key=index, force=force)

            if 'error' in result:
                raise

            return result

    # Parse Operator
    parse_operator_result = parse_operator(next_tag)
    next_tag, operator = parse_operator_result

    if operator is not None:
        if operator == sub_data:
            return {'result': True, 'parent': parent, 'parent_key': parent_key, 'next_tag': next_tag}
        else:
            #if force:
            #    parent[parent_key] = operator
            #     if isinstance(parent, dict):
            #         parent[parent.popitem()[0]] = operator
            #     elif isinstance(parent, list):
            #         parent
            #     return {'result': True, 'next_tag': next_tag}

            return {'result': False, 'parent': parent, 'parent_key': parent_key, 'next_tag': next_tag}

    if next_tag == "":
        return {'result': sub_data, 'parent': parent, 'parent_key': parent_key, 'next_tag': next_tag}
    else:
        return {'result': None, 'parent': parent, 'parent_key': parent_key, 'next_tag': next_tag, 'error': 'Cannot parse'}



if __name__ == "__main__":
    #print(write_json_file("test.json", {'a': [1, 2, 3], 'b': [{'s': 'test', 't': 'test2'}]}, indent=2))
    #print(write_json_file("test.json", {'a': [1, 2, 3], 'b': [{'s': 'test', 't': 'test2'}]}, compact=True))

    try:
        read_json_file("malformed.json")
    except:
        pass

    data = read_json_file("test.json")
    #print(data)
    print(get_value("b[-]", read_json_string('{"b":[5,6]}')))
    assert get_value("b[1]", read_json_string('{"a":1}'))['result'] is None
    #print(get_value("a[0]=1", read_json_string('{"a":[1]}')))
    assert get_value("a[0]=1", read_json_string('{"a":[1]}'))['result'] is True
    #print(get_value("a[0]=1", read_json_string('{"a":[2]}')))
    assert get_value("a[0]=1", read_json_string('{"a":[2]}'))['result'] is False
    assert get_value("a=1", read_json_string('{"a":1}'))['result'] is True
    assert get_value("[0]b[1]", read_json_string('{"a":1}'))['result'] is None
    assert get_value("b[1]", read_json_string('{"a":1}'))['result'] is None
    assert get_value("b[1]", read_json_string('{"b": [0]}'))['result'] is None
#    print(get_value("b[1]", read_json_string('{"b": [0,1]}')))
    assert get_value("b[1]", read_json_string('{"b": [0,1]}'))['result'] == 1
    #print(get_value("b[?]", read_json_string('{"b": [{"a": 1}, {"b": 1}]}')))
    print(get_value("b[?]", read_json_string('{"b": [{"a": 1}, {"b": 1}]}'))['results'][1]['result'])
    assert get_value("b[?]", read_json_string('{"b": [{"a": 1}, {"b": 1}]}'))['results'][1]['result'] == OrderedDict([("b", 1)])
    print(get_value("b[?]", read_json_string('{"b": [0, 1]}'))['results'])
    assert get_value("b[?]", read_json_string('{"b": [0, 1]}'))['results'][0]['parent'] == [0, 1]
#    assert get_value("b[?>=1]", read_json_string('{"b": [0, 5]}'))['results'] == [5]
#    assert get_value("b[?>=1].f", read_json_string('{"b": [0, {"f": 5}]}'))['result'] == [5]
#    assert get_value("b[?>=1].e", read_json_string('{"b": [0, {"f": 5}]}'))['result'] == []
#    assert get_value("b[?>=1]", read_json_string('{"b": [0, {"f": 5}]}'))['result'] == [{"f": 5}]
#    assert get_value("b[?<1]", read_json_string('{"b": [0, 1]}'))['result'] == [0]
#    assert get_value("b[?==1]", read_json_string('{"b": [0, 1]}'))['result'] == [1]
#    assert get_value("b[? == 1]", read_json_string('{"b": [0, 1]}'))['result'] == [1]
#    assert get_value("b[? == 1]", read_json_string('{"b": [0, {"d": [1, 2]}]}'))['result'] == [{"d": [1, 2]}]
    assert get_value("b.c.d", read_json_string('{"b": {"c":{"d": 1}}}'))['result'] == 1
    #print(get_value("b.?.d", read_json_string('{"b": {"c": {"d": 1}, "e": {"d": 1}, "f": {"e": 1}}}')))
#    assert get_value("b.?.d", read_json_string('{"b": {"c": {"d": 1}, "e": {"d": 1}, "f": {"e": 1}}}'))['result'] == [1, 1]
#    assert get_value("b.?[0].d", read_json_string('{"b": {"c": [{"d": 1}]}}'))['result'] == [1]
#    assert get_value("?.d", read_json_string('{"a": {"d": 1}, "b": {"d": [1, 2]}, "c": {"d": {"f": 3}}}'))['result'] == [1, [1, 2], {"f": 3}]
    print("->", get_value("b{[1]=5}", read_json_string('{"b":[0,5]}')))
    assert get_value("b{[1]=5}", read_json_string('{"b":[0,5]}'))['result'] == [0, 5]
    assert get_value("a{d=1}.b", read_json_string('{"a": {"d": 1, "b": 3}}'))['result'] == 3
    print("->", get_value("a{d=5}.b", read_json_string('{"a": {"d": 1, "b": 3}}')))
    assert get_value("a{d=5}.b", read_json_string('{"a": {"d": 1, "b": 3}}'))['result'] is None
    assert get_value("a{[0]}[1]", read_json_string('{"a": [5, 3]}'))['result'] == 3
    assert get_value("a{[0]}{[1]=3}[2]", read_json_string('{"a": [5, 3, 7]}'))['result'] == 7
#    assert get_value("a{[?]}{[1]}[2]", read_json_string('{"a": [5, 0, 7]}'))['result'] == 7
 #   assert get_value("a{?=4}", read_json_string('{"a": {"b": 5, "c": 2, "d": 4}}'))['result'] == OrderedDict([('b', 5), ('c', 2), ('d', 4)])
#    assert get_value("a.?{='test'}", read_json_string('{"a": {"b": "test", "c": 2, "d": "test"}}'))['result'] == ['test', 'test']
#    assert get_value("a.?{=4}", read_json_string('{"a": {"b": 5, "c": 2, "d": 4}}'))['result'] == [4]
#    assert get_value("a.?", read_json_string('{"a": {"b": 5, "c": 2, "d": 4}}'))['result'] == [5, 2, 4]
#    assert get_value("a.?{z=4}", read_json_string('{"a": {"b": 5, "c": {"z": 4, "k": 6}, "d": 4}}'))['result'] == [{"z": 4, "k": 6}]
    print(get_value("a=1", read_json_string('{}'), force=True))

    d = {}
    get_value("a=1", d, force=True)
    print(d)

    print(get_value("a=1", read_json_string('{}'), force=True))
    assert get_value("a=1", read_json_string('{}'), force=True)['result'] == False
    print(get_value("a.b", read_json_string('{}'), force=True)['result'])
    d = {}
    assert get_value("a.b", d, force=True)['result'] == {}
    print(d)
    print(get_value("b[0]", read_json_string('{}'), force=True))
    assert get_value("b[0]", read_json_string('{}'), force=True)['result'] is None
    d = {'a':[1]}
    assert get_value("b[2]", d, force=True)['result'] is None
    print(d)
    assert d == {'b': [None, None, None], 'a': [1]}
    #print(get_value("a.b[3]=4", read_json_string('{}'), force=True))
    assert get_value("a.b[3]", read_json_string('{}'), force=True)['result'] is None
    #print(get_value("a.b[3]=4", read_json_string('{}'), force=True))
    assert get_value("a.b[3]=4", read_json_string('{}'), force=True)['result'] is False

    d = {}
    assert get_value("a[+]", d, force=True)['result'] is None
    print(d)
    assert d == {'a': [None]}
    assert get_value("a[+]", d, force=True)['result'] is None
    print(d)
    assert d == {'a': [None, None]}

    d = {'a': [{'key': "T(0)"}, {'key': "T(01"}, {'key': "T(1)"}]}
    assert get_value("a[?]{key='T\(1\)'}", d, force=True)

    print("LLL", get_value("a[?]{key='T(1)'}", d, force=True))

    d = {'tracks':[{'a':1},{'b':2}]}
    r = get_value("tracks[+]", d, force=True)['result']
    print(r)
    #assert r is False
    print(d)

    d = {'t': [{'key':'a', 'val': 1}, {'key': 'a', 'val': 2}]}
    r = get_value("t[?]{key = 'a'}", d, force=True)
    print(r['results'][1]['result'], d)

    """
    a -> b -> [2] -> {='tet'}
    {b :[..,..,'tet]]} -> [..'tet'] -> 'tet
    """
