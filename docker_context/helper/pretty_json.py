#!/usr/bin/env python3

"""
Helper Script which takes a json file. It sort keys by name and add a nice indent to make more readable.
"""

import sys
import json

with open(sys.argv[2], 'w') as f_out, open(sys.argv[1]) as f_in:
    try:
        data = json.load(f_in)
    except json.JSONDecodeError:
        print("File empty or not a json")
        exit(-1)

    json.dump(data, f_out, sort_keys=True, indent=2)


