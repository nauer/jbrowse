#!/usr/bin/env python3

import sys
import re

pattern = re.compile("(^\S*)[^=]*=(.*)]")

with open(sys.argv[2],'w') as f_out:
    with open(sys.argv[1]) as f_in:
        for line in f_in:
            if line.startswith(">"):
                res = pattern.search(line)

                print(res.group(0))

                f_out.write("{}_{}\n".format(res.group(1), res.group(2)))
            else:
                f_out.write(line)