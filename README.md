# Build cgr-referencegenome.boku.ac.at
Updating or building a new JBrowse instance is divided into 2 steps.
+ Create a new JBrowse container with the newest JBrowse version
+ Run the build script with the newest data

## How to build the JBrowse container
First get the lastest `git` repo from the ACIB server
~~~bash
git clone https://gitlab.com/nauer/jbrowse.git
~~~

or update to the latest version.

~~~bash
cd JBrowse-Hamster
git pull
~~~

Check the README.md for more information.

### Find out latest JBrowse version
If you run the `docker build` command without the wished JBrowse version it shows all available versions

~~~bash
cd JBrowse-Hamster
docker build --file docker_context/Dockerfile.jbrowse -- docker_context
~~~

or go to the official JBrowse site [JBrowse](https://jbrowse.org/) and check latest version.

<div style="page-break-after: always;"></div>
### Build specific JBrowse version
Select one version from the previous command and run the build command again.

~~~bash
# Set JB Version
JB_VERSION=1.16.3-release

# Build new JBrowse Docker Image
docker build --pull --no-cache --force-rm --build-arg USERID=$(id -u $USER) \
  --build-arg JBROWSE_VERSION=$JB_VERSION -t nauer/jbrowse:$JB_VERSION \
  --file docker_context/Dockerfile.jbrowse -- docker_context
~~~

## Start a new JBrowse instance
The JBrowse-Hamster package includes a `create-jbrowse-project.py` script for building a complete project skeleton for new JBrowse
projects to run stand alone in a Docker environment. Run following command with the target project folder.

~~~bash
./create-jbrowse-project.py <PROJECT_FOLDER>
~~~


<img src="overview.png" alt="Overview" width="400"/>


### Configuration
Now switch to the new project folder and start configure the project. Open the `.env` file and adapt variables.
~~~
# Port where Apache is delivering the JBrowse e.g. http://localhost:11000/jb.
# Choose a free one
APACHE_PORT=11000

# JBrowse Version to use (Must be build before)
JBROWSE_VERSION=1.16.3-release

# Apache Docker Image version
APACHE_VERSION=7.3.3-apache

# Docker environment variables
COMPOSE_HTTP_TIMEOUT=200

# Name is used for container names
PROJECT_NAME=cgr-referencegenome
~~~

### Start Docker container
Now it is time to start the containers
~~~bash
# To start in foreground
docker-compose up

# or in background
docker-compose up -d
~~~

When both containers are running it is now possible to connect to the Apache webserver running at the port set in `.env` -  http://localhost:<APACHE_PORT>

### Update the build_jb.sh script
For building your own instance use a build script. This makes it easy to update your JBrowse instance if new data comes in or if a new JBrowse version is available.
Put all scripts into the `./scripts` folder. This folder is bound into `/scripts` on the container and is in the `$PATH` variable.

To execute a script in the container use `docker-compose exec <container> <command>` command.

~~~bash
docker-compose exec jbrowse build_jb.sh
~~~


### Where is what
+ `/jb` - Here are the JBrowse files after the build. Copy this folder to the public server
+ `/data` - Put your original data files (.fa, .bed, .gff3, etc.) in this folder
+ `/scripts` - Put your build scripts and helper scripts. This folder is in $PATH in the container
+ `/logs` - Write your logs into /logs/progress. Docker is using this file for logging
+ `/configs` - Put your jbrowse.conf and jbrowse_conf.json into this folder

## Update a JBrowse instance to the newest JBrowse version
Switch to the project folder and update .env file for the newer JBrowse version. Restart the docker containers to switch to the new JBrowse container.

~~~bash
# Remove all containers
docker-compose down

# Start/Update containers
docker-compose up -d
~~~

Run the build script again

~~~bash
docker-compose exec jbrowse build_jb.sh
~~~

## Misc
### TODO
Use `https://` instead of http://. Check certbot for this.
Since version 1.14.0 colors from bed files can be used if `ItemRgb` is set to `true`. Maybe better than the used `color` function. But bed files have to be adjusted for it.

